---
layout: page
title: "Informations"
image: assets/images/annexe-relais59-epn-paris12.jpeg
---

Nous vous recevons sans rendez-vous du lundi au vendredi entre 10h et 12h pour notre permanence d'accueil et d'inscriptions. À cette occasion nous faisons le point selon vos besoins et notre offre pour vous proposer la formation informatique la plus adaptée.

# Tarifs
Nos formations sont à 5€ de l'heure (fonction du nombre d'heures que représente la durée de la formation). Des tarifs dégressifs sont également proposés pour les demandeurs d'emploi, bénéficiaires de minima sociaux, etc.

# Accès libres
Nos ordinateurs sont en accès libres **les lundis et mercredis entre 10h et 12h**. L'accès libre est entièrement gratuit, nous demandons seulement 10c par impression (seul le noir et blanc est disponible).

Nous avons également trois scanners et deux bornes tactiles.

# Permanence administrative
Une bénévole vous reçoit tous les mercredis de 16h à 19h (nous arrêtons de recevoir à partir de 18h) afin de vous aider dans vos démarches administratives en ligne. Pour toute autre demande administrative, consultez [les écrivains publics du Relais 59](http://csrelais59.org/accueilep).

# Comment venir
L’espace numérique est situé au 8, place Henri Frenay, Paris.

## Si vous passez par le boulevard Diderot
Entre les numéros 28 et 30 du bd Diderot se trouve une sorte de placette avec des platanes et des réverbères. En suivant les réverbères, vous vous dirigerez vers la place Henri Frenay. Longez le magasin de Hi-Fi Elecson et tournez à gauche après le bâtiment : notre local est situé sous les arcades (vous apercevrez notre enseigne « Relais 59 – Esp@ce Numérique »).

## Si vous passez par l’avenue Daumesnil
A la hauteur du 44 avenue Daumesnil, prenez la rue Guillaumot jusqu’au bout, traversez la rue Jean Bouton et passez le porche qui donne sur la place Henri Frenay. Après le porche, dirigez-vous vers l’extrême droite de la place, notre local est situé à la fin des arcades.

## Transports
**Gare de Lyon**
* Métro : ligne 1, sortie rue de Chalon (située sur le Bd Diderot) et ligne 14, sortie n°9 place Henri Frenay
* RER : lignes A et D du RER, sortie n°9 place Henri Frenay
* Bus : lignes 20, 57, 63 et 65

![plan](assets/images/plan-annexe.png)
