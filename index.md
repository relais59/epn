---
layout: default
---

<!-- Section -->
<section>
	<header class="major">
		<h2>Nos missions</h2>
	</header>
	<div class="features">
		<article>
			<span class="icon fa-diamond"></span>
			<div class="content">
				<h3>Apprentissage de l'informatique</h3>
				<p>Se rendre autonome en apprenant pas à pas les usages informatiques courants.</p>
			</div>
		</article>
		<article>
			<span class="icon fa-paper-plane"></span>
			<div class="content">
				<h3>Accéder à internet</h3>
				<p>Pouvoir consulter ses mails et des offres d'emploi.</p>
			</div>
		</article>
		<article>
			<span class="icon fa-rocket"></span>
			<div class="content">
				<h3>Repenser le numérique</h3>
				<p>Programmer, approfondir, découvrir, et repenser le numérique sans ses GAFAs.</p>
			</div>
		</article>
		<article>
			<span class="icon fa-signal"></span>
			<div class="content">
				<h3>Se faire accompagner</h3>
				<p>Se faire accompagner dans les démarches administratives en ligne.</p>
			</div>
		</article>
	</div>
</section>

<!-- Section -->
<section>
	<header class="major">
		<h2>Le Relais 59</h2>
	</header>
<p>Cet espace numérique est porté par un centre social, le Relais 59. Pour en savoir plus, <a href="http://csrelais59.org">rendez-vous sur le site</a>.</p>
</section>
