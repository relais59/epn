# Mister Jekyll 
[![pipeline status](https://gitlab.com/relais59/epn/badges/master/pipeline.svg)](https://gitlab.com/relais59/epn/commits/master) [![coverage report](https://gitlab.com/relais59/epn/badges/master/coverage.svg)](https://gitlab.com/relais59/epn/commits/master)
*Site temporaire de l'@nnexe du Relais 59 réalisé avec GitLab.*

Pour consulter le front de ce site, visitez la page [epn.csrelais59.org](http://www.csrelais59.org/epn)

Pour faire ce site, j'ai forké un thème de HTML5 UP (sous licence GNU/GPL v3) et utilisé le présent GitLab et [Jekyll](https://jekyllrb.com/). Jekyll est un framework en Ruby extrêmement simple d'utilisation qui permet de générer des sites statiques (sans base de donnée). Vous trouverez dans le wiki un tutoriel pour réaliser votre propre site, avec votre nom de domaine, en utilisant uniquement GitLab.

# Comment forker

Si vous voulez utiliser le même code et le bidouiller à votre sauce, c'est très facile. 

Il faut d'abord installer Ruby, Jekyll et les dépendances qui vont bien. 

On vérifie d'abord si ruby est installé, et sa version. S'il n'est pas installé, on procède, et enfin on installe Jekyll.

```
ruby -v
sudo apt install ruby 
gem install bundler jekyll 
```
Pour Windows, vous pouvez suivre [la doc officielle](https://jekyllrb.com/docs/windows/).

Maintenant vous pouvez cloner ce repo avec git

```
git clone git@gitlab.com:relais59/epn.git
cd epn/
```

# Comment lancer la bête

Pas besoin d'apache lamp, wamp et autre installation complexe. Si vous avez respecté les étapes précédentes, vous pouvez simplement lancer le serveur en lançant :

```
jekyll serve
```

Ensuite, lancez la page [localhost:4000](http://localhost:4000).

**TSADA !**

![TSADA!](https://i.giphy.com/media/l4HodBpDmoMA5p9bG/giphy.webp "TSADA!")

Faites vos bidouilles avec votre éditeur préféré. Et pour mettre à jour votre code et le renvoyer vers GitLab :

```
git remote add origin git@gitlab.com:PSEUDO/REPO.git
git add .
git commit -m "Mon premier commit *.*"
git push -u origin master
````

# Aller plus loin

Dans le wiki prochainement, quelques tutos : 
- [x] lien vers le wiki : [ici](https://gitlab.com/relais59/epn/wikis/accueil)
- [ ] page wiki : déployer une page frontend du code source sur gitlab
- [ ] page wiki : utiliser un nom de domaine custom sur sa page gitlab
- [ ] page wiki : utiliser sass 

Bref, expliquer comment faire tout comme ce présent site...

# Credits

README Original de HTML5 UP:

```
Editorial by HTML5 UP
html5up.net | @ajlkn
Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)


Say hello to Editorial, a blog/magazine-ish template built around a toggleable "locking"
sidebar (scroll down to see what I mean) and an accordion-style menu. Not the usual landing
page/portfolio affair you'd expect to see at HTML5 UP, but I figured for my 41st (!!!)
template I'd change it up a little. Enjoy :)

Demo images* courtesy of Unsplash, a radtastic collection of CC0 (public domain) images
you can use for pretty much whatever.

(* = not included)

AJ
aj@lkn.io | @ajlkn


Credits:

	Demo Images:
		Unsplash (unsplash.com)

	Icons:
		Font Awesome (fortawesome.github.com/Font-Awesome)

	Other:
		jQuery (jquery.com)
		html5shiv.js (@afarkas @jdalton @jon_neal @rem)
		Misc. Sass functions (@HugoGiraudel)
		Respond.js (j.mp/respondjs)
		Skel (skel.io)
```

Repository [Jekyll logo](https://github.com/jekyll/brand) icon licensed under a [Creative Commons Attribution 4.0 International License](http://choosealicense.com/licenses/cc-by-4.0/).
